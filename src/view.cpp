#include "view.h"
#include <QApplication>
#include <QKeyEvent>

View::View(QWidget *parent) : QGLWidget(parent)
{
    // View needs all mouse move events, not just mouse drag events
    setMouseTracking(true);

    // Hide the cursor since this is a fullscreen app
    setCursor(Qt::BlankCursor);

    // View needs keyboard focus
    setFocusPolicy(Qt::StrongFocus);

    // The game loop is implemented using a timer
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));

    m_Game = new Game();
    m_camera = new Camera();
    m_graphics = new Graphics(this);

    fps = 0;
}

View::~View()
{
    delete m_Game;
    delete m_camera;
    delete m_graphics;
}

void View::initializeGL()
{
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    // All OpenGL initialization *MUST* be done during or after this
    // method. Before this method is called, there is no active OpenGL
    // context and all OpenGL calls have no effect.

    // Start a timer that will try to get 60 frames per second (the actual
    // frame rate depends on the operating system and other running programs)
    time.start();
    timer.start(1000 / 60);

    // Center the mouse, which is explained more in mouseMoveEvent() below.
    // This needs to be done here because the mouse may be initially outside
    // the fullscreen window and will not automatically receive mouse move
    // events. This occurs if there are two monitors and the mouse is on the
    // secondary monitor.
    QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
    m_graphics->screen_height = height();
    m_graphics->screen_width = width();

    m_graphics->loadTexture(":/entity/smileyface.jpg", 1, 1);
    m_graphics->loadTexture(":/entity/angry_face.png", 1, 1);
    m_graphics->loadTexture(":/entity/bullet.jpg", 1, 1);
    m_graphics->loadTexture(":/block/terrain.png", 16, 16);
    m_graphics->loadTexture(":/block/intersectface.png", 1, 1);

    m_Game->onInitialize();

}

void View::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // TODO: call your game rendering code here

    glEnable(GL_DEPTH_TEST);
    m_camera->lookAt((float)width(), (float)height());

    m_graphics->drawIntersectionPoint(m_camera->eye + m_camera->look);
    // get the product matrix for view frustum culling
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    GLfloat matrix_mv[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, matrix_mv);
    glMultMatrixf(matrix_mv);

    GLfloat matrix_product[16];
    glGetFloatv(GL_PROJECTION_MATRIX, matrix_product);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    m_Game->onDraw(m_graphics, matrix_product);
    glColor3f(1,1,1);
    renderText(10, 20, "FPS: " + QString::number((int) (fps)), this->font());
}

void View::resizeGL(int w, int h)
{
    m_Game->onResize(w, h, m_camera);

    m_graphics->screen_height = h;
    m_graphics->screen_width = w;
}

void View::mousePressEvent(QMouseEvent *event)
{
    m_Game->onMousePressEvent(event);
}

void View::mouseMoveEvent(QMouseEvent *event)
{
    // This starter code implements mouse capture, which gives the change in
    // mouse position since the last mouse movement. The mouse needs to be
    // recentered after every movement because it might otherwise run into
    // the edge of the screen, which would stop the user from moving further
    // in that direction. Note that it is important to check that deltaX and
    // deltaY are not zero before recentering the mouse, otherwise there will
    // be an infinite loop of mouse move events.
    int deltaX = event->x() - width() / 2;
    int deltaY = event->y() - height() / 2;
    if (!deltaX && !deltaY) return;
    QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));

    // TODO: Handle mouse movements here    
    m_Game->onMouseMoveEvent(deltaX, deltaY, m_camera);
}

void View::mouseReleaseEvent(QMouseEvent *event)
{
    m_Game->onMouseReleaseEvent(event);
}

void View::wheelEvent(QWheelEvent *event)
{

}

void View::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape) QApplication::quit();

    // TODO: Handle keyboard presses here
    if (event->key() == Qt::Key_Shift)
        m_camera->mode = (m_camera->mode + 1) % 2;

    m_Game->onKeyPressEvent(event);
}

void View::keyReleaseEvent(QKeyEvent *event)
{
    m_Game->onKeyReleaseEvent(event);
}

void View::tick()
{
    // Get the number of seconds since the last tick (variable update rate)
    float seconds = time.restart() * 0.001f;
    fps = .02f / seconds + .98f * fps;

    // TODO: Implement the game update here
    seconds = 0.017;
    m_Game->onTick(seconds, m_camera);

    // Flag this view for repainting (Qt will call paintGL() soon after)
    update();
}
