#include "minecraftvoxelmanager.h"

MinecraftVoxelManager::MinecraftVoxelManager()
{
    texTable = new BlockTexCoor[256];
}

MinecraftVoxelManager::~MinecraftVoxelManager()
{
    delete[] texTable;
}

void MinecraftVoxelManager::generateTexTable()
{
    BlockTexCoor txc;

    //grass
    txc.top = findoutCoorByIdx(1,1);
    txc.bot = findoutCoorByIdx(3,1);
    txc.bdy = findoutCoorByIdx(4,1);
    texTable[GRASS] = txc;

    //dirt
    txc.top = findoutCoorByIdx(3,1);
    txc.bot = txc.top;
    txc.bdy = txc.top;
    texTable[DIRT] = txc;

    //WATER
    txc.top = findoutCoorByIdx(16,13);
    txc.bot = findoutCoorByIdx(15,13);
    txc.bdy = findoutCoorByIdx(14,13);
    texTable[WATER] = txc;

    //stone
    txc.top = findoutCoorByIdx(2,1);
    txc.bot = txc.top;
    txc.bdy = txc.top;
    texTable[STONE] = txc;

    //snowongrass
    txc.top = findoutCoorByIdx(3,5);
    txc.bot = findoutCoorByIdx(3,1);
    txc.bdy = findoutCoorByIdx(5,5);
    texTable[SNOWONGRASS] = txc;
}

Chunk* MinecraftVoxelManager::generateAChunk( QPair<QPair<int, int>, int> chkidx)
{
    int water_x, water_z;
    //first pass: generate type for each block

    Chunk *chk = new Chunk();

    int xn = chkidx.first.first;
    int yn = chkidx.first.second;
    int zn = chkidx.second;

    for(int  z= 0; z < CHUNK_SIZE; z++)
        for(int x =0; x < CHUNK_SIZE; x++)
        {
            Vector3 worldcoor;
            worldcoor.x = x + CHUNK_SIZE * xn;
            worldcoor.z = z + CHUNK_SIZE * zn;
            float h = valueNoise2D(worldcoor.x / 64.0, worldcoor.z / 64.0);

            for(int y = 0; y < CHUNK_SIZE; y++)
            {
                worldcoor.y = y + CHUNK_SIZE * yn;

                int blkidx = getBlockIdx(x, y, z);

                if(yn < 0)
                {
                    if( y == CHUNK_SIZE - 1)
                        chk->_blocks[blkidx].type = STONE;
                    else if( worldcoor.y < (h-0.2) * CHUNK_SIZE)
                        chk->_blocks[blkidx].type = 0;
                    else
                        chk->_blocks[blkidx].type = STONE;
                }
                else
                {
                    if( y == 0 && yn == 0)
                        chk->_blocks[blkidx].type = GRASS;
                    else if( worldcoor.y > 0.8 * h * CHUNK_SIZE)
                        chk->_blocks[blkidx].type = 0;
                    else if ( worldcoor.y > 0.5f * CHUNK_SIZE)
                        chk->_blocks[blkidx].type = SNOWONGRASS;
                    else
                        chk->_blocks[blkidx].type = GRASS;
                }

                //first two bits
                chk->setBlockTPProperty(x, y, z);

            }
        }
    //make a WATER, calculate the random coordinates
    if( yn == 0)
    {
        srand((pow(chkidx.first.first, 2) + pow(chkidx.first.second, 2) + pow(chkidx.second, 2))/1.5);
        water_x = rand() % 30 + 1; // 1 - 30
        srand(pow(chkidx.first.first, 2) + pow(chkidx.first.second, 2) + pow(chkidx.second, 2));
        water_z = rand() % 30 + 1; // 1 - 30
        //                    cout<<"water center: "<<water_x<< ", "<<water_z<<endl;
        makeALake(chk, water_x, water_z);
    }

    //second pass: assign visibility for each block face
    for(int  z= 0; z < CHUNK_SIZE; z++)
        for(int x =0; x < CHUNK_SIZE; x++)
            for(int y =0; y < CHUNK_SIZE; y++)
            {
                chk->setBlockFaceProperty(x, y, z);
            }
    //After the property is set, init the vbo
    chk->initVbo(texTable);

    return chk;
}

void MinecraftVoxelManager::makeALake(Chunk *c, int x, int z)
{
    for( int i = x - 1; i < x + 2; i ++)
        for( int j = z - 1; j < z + 2; j ++)
        {
            int blkidx = getBlockIdx(i, 0, j);
            c->_blocks[blkidx].type = WATER;
            c->_blocks[blkidx].property = 0b11000000;
        }
}


