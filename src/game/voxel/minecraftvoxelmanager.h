#ifndef MINECRAFTVOXELMANAGER_H
#define MINECRAFTVOXELMANAGER_H
#include "engine/voxel/voxelmanager.h"

class MinecraftVoxelManager : public VoxelManager
{
public:
    MinecraftVoxelManager();
    ~MinecraftVoxelManager();

    virtual void generateTexTable();
    virtual Chunk *generateAChunk(QPair<QPair<int, int>, int> chkidx);

    void makeALake(Chunk *c, int x, int z);

//    virtual float collisionDetectX(Vector3 pos, float radius, Vector3 nextposition);
//    virtual float collisionDetectZ(Vector3 pos, float radius, Vector3 nextposition);
//    virtual float collisionDetectY(Vector3 pos, float &height, Entity *player);
};

inline Vector2 findoutCoorByIdx(int x, int y)
{
    Vector2 tl;
    tl.x = 0.0625*(x - 1);
    tl.y = 1 - 0.0625*(y - 1);
    return tl;
}

#endif // MINECRAFTVOXELMANAGER_H
