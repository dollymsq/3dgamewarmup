#include "bullet.h"

Bullet::Bullet(Vector3 p, Vector3 d): Entity(p, d)
{
    speed = 20;
    textureStr = ":/entity/bullet.jpg";
    vxz = Vector3(0,0,0);
    blood = 1;

    identity = BULLET;
}

void Bullet::onTick(float seconds)
{
    inair = true;
    MoveInXZPlane(seconds);
    Jump(seconds);
}

void Bullet::MoveInXZPlane(float t)
{
    pos = pos + t * vxz;
}

void Bullet::setDirection(Camera *cam, Vector3 v)
{
    if(vxz == Vector3(0,0,0))
    {
        vxz.x = cam->look.x * speed;
        vxz.z = cam->look.z * speed;

        vup = cam->look.y * speed;
    }
}

void Bullet::onCollide(Entity *e, Vector3 mtv)
{
    blood = max(0, blood -1);
    pos -= mtv/2;
    e->blood = max(0, blood -1);
    e->pos += mtv/2;
}
