#include "player.h"

Player::Player(Vector3 p, Vector3 d): Entity(p, d)
{
    vupMax = 4;
    speed = 30;
    textureStr = ":/entity/smileyface.jpg";

    blood = 3;
    score = 0;
    identity = PLAYER;
}

void Player::onTick(float seconds)
{
    inair = true;
    MoveInXZPlane(seconds);
    Jump(seconds);
}

void Player::MoveInXZPlane(float t)
{
    Vector3 vgoal = vxz * speed;
    Vector3 a = 0.2 * (vgoal - vxz);
    vxz += a;
    pos = pos + t * vxz;
}

void Player::setDirection(Camera *cam, Vector3 v)
{
    vxz = v;
}

void Player::onCollide(Entity *e, Vector3 mtv)
{
    if(e->identity == BULLET)
    {
        pos -= mtv/2;
        e->pos += mtv/2;

        blood = max(0, blood -1);
        e->blood = max(0, blood -1);
    }
    else if(e->identity == ENEMY)
    {
        pos -= mtv/3 * 2;
        e->pos += mtv/3 * 2;

        blood = max(0, blood -1);
        e->isfrozen = true;
        e->vxz = Vector3(0, 0, 0);
    }
}
