#include "enemy.h"

Enemy::Enemy(Vector3 p, Vector3 d): Entity(p, d)
{
    speed = 12;
    textureStr = ":/entity/angry_face.png";
    blood = 2;

    identity = ENEMY;
    isfrozen = false;
}

void Enemy::onTick(float seconds)
{
    inair = true;
    MoveInXZPlane(seconds);
    MoveInYDirection(seconds);
    Jump(seconds);
}

void Enemy::MoveInXZPlane(float t)
{
    pos = pos + t * vxz;
}

void Enemy::setDirection(Camera *cam, Vector3 v)
{
    if(!isfrozen)
        vxz = v;
}

void Enemy::onCollide(Entity *e, Vector3 mtv)
{
    pos -= mtv/3 * 2;
    e->pos += mtv/3 * 2;

    if(e->identity == PLAYER)
    {
        e->blood = max(0, blood -1);
        vxz = Vector3(0, 0, 0);
        isfrozen = true;
    }
    else if(e->identity == BULLET)
    {
        blood = max(0, blood -1);
        e->blood = max(0, blood -1);
    }
}
