#ifndef PLAYER_H
#define PLAYER_H
#include "engine/world/entity.h"

class Player : public Entity
{
public:
    Player(Vector3 p, Vector3 d);

    virtual void onTick(float seconds);

    virtual void MoveInXZPlane(float t);
    virtual void setDirection(Camera *cam, Vector3 v);
    virtual void onCollide(Entity *e, Vector3 mtv);

};

#endif // PLAYER_H
