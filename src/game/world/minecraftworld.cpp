#include "minecraftworld.h"

MinecraftWorld::MinecraftWorld()
{
    m_voxelmanager = NULL;
}

void MinecraftWorld::onTick(float seconds, Camera *cam)
{
    look = cam->look;

//    //check whether the player is dead, if so, restart the game
//    if(_entities[0]->blood <= 0)
//        restart();

    //remove dead bodies except the player
    foreach(Entity *e, _entities)
    {
        if(e->blood <= 0 )
        {
            if(e->identity != PLAYER)
                _entities.removeOne(e);
            if(e->identity == ENEMY)
                _entities[0]->score += 1;
        }
    }

    //player move direction set
    foreach(Entity *e, _entities)
    {
        e->setDirection(cam, (_entities[0]->pos - e->pos).unit());
    }
    _entities[0]->setDirection(cam, cam->forward * fb + cam->right * lr);

    //move and detect collision with the environment
    foreach(Entity *e, _entities)
    {
        Vector3 prepos = e->pos;
        e->onTick(seconds);
        Vector3 nextpos = e->pos;

        e->pos = m_voxelmanager->collisionDetect(prepos, e, nextpos);
    }
    //collision between entities
    collisionDetectBtwnEntites();

    //set the cam's position the same as eye
    cam->setEye(_entities[0]->pos);

    //find which block is selected, return the selectedblkidx and the direction.
    cameraIntersect = m_voxelmanager->rayCasting(cam->eye, cam->look);

    //set the playerpos in voxelmanager, it would need this to load chunks
    m_voxelmanager->playerPos = _entities[0]->pos;

    //to load or unload chunks
    m_voxelmanager->handleChunksToLoad();
}

void MinecraftWorld::onDraw(Graphics *g, GLfloat *matrix)
{
    World::onDraw(g, matrix);
    m_voxelmanager->onDraw(g, matrix, cameraIntersect);
    g->renderText(10, 60, 12, "Position: ( " + QString::number((double) (_entities[0]->pos.x))
                  + ", " + QString::number((double) (_entities[0]->pos.y))
                  + ", " + QString::number((double) (_entities[0]->pos.z)) + ")");
    g->renderText(10, 90, 16, "Blood left: "+ QString::number(_entities[0]->blood)
                  + " / " + QString::number(3));
    g->renderText(10, 120, 16, "Your score: "+ QString::number(_entities[0]->score));

    if(!isPlayerAlive())
        g->renderText(10, 200, 20, "You died, press any key to restart the game.");

}

int MinecraftWorld::onKeyPressEvent(QKeyEvent *event)
{
    if(isPlayerAlive())
    {
        if(event->key() == Qt::Key_Space)
        {
            if(!_entities[0]->inair)
            {
                _entities[0]->vup = _entities[0]->vupMax;
                _entities[0]->inair = true;
            }
        }
        else if(event->key() == Qt::Key_D)
            lr = 1;
        else if(event->key() == Qt::Key_A)
            lr = -1;
        else if(event->key() == Qt::Key_W)
            fb = 1;
        else if(event->key() == Qt::Key_S)
            fb = -1;
        else if(event->key() == Qt::Key_Q) // shoot
            addEntity(new Bullet(_entities[0]->pos + 0.5*look + Vector3(0, _entities[0]->dim.y, 0),
                                 Vector3(0.2, 0.2, 0.2)));
        else if(event->key() == Qt::Key_R)
            return 2;
//        else if(event->key() == Qt::Key_Z)
//        {
//            if(cameraIntersect.exist)

//            {
//                Vector3 toRemoveBlkCoor = cameraIntersect.selectedBlkCoor;
//                m_voxelmanager->removeOneBlock(toRemoveBlkCoor);
//            }
//        }

//        for debug
//        else if(event->key() == Qt::Key_Z)
//            lr = 0;

//            if(!_entities[0]->inair)
//            {
//                if(event->key() == Qt::Key_U)
//                    ud = 1;
//                else if(event->key() == Qt::Key_N)
//                    ud = -1;
//            }

        return 1;
    }
    else
        return 2;

}

void MinecraftWorld::onKeyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_D || event->key() == Qt::Key_A)
        lr = 0;
    else if(event->key() == Qt::Key_W || event->key() == Qt::Key_S)
        fb = 0;
    else if(event->key() == Qt::Key_U || event->key() == Qt::Key_N)
        ud =  0;

}

void MinecraftWorld::onMousePressEvent(QMouseEvent *event)
{
    if(cameraIntersect.exist)
    {
        if(event->button() == Qt::LeftButton) // add a block at that point
        {
            Vector3 toAddBlkCoor = cameraIntersect.selectedBlkCoor;
            switch(cameraIntersect.faceHit)
            {
            case YPOS:
                toAddBlkCoor.y +=1;
                break;
            case YNEG:
                toAddBlkCoor.y -=1;
                break;
            case XPOS:
                toAddBlkCoor.x +=1;
                break;
            case XNEG:
                toAddBlkCoor.x -=1;
                break;
            case ZPOS:
                toAddBlkCoor.z +=1;
                break;
            case ZNEG:
                toAddBlkCoor.z -=1;
                break;
            default:
                break;
            }
            m_voxelmanager->addOneBlock(toAddBlkCoor, SNOWONGRASS);
        }
        else if(event->button() == Qt::RightButton)
        {
            Vector3 toRemoveBlkCoor = cameraIntersect.selectedBlkCoor;
            m_voxelmanager->removeOneBlock(toRemoveBlkCoor);
        }
    }
}

void MinecraftWorld::onMouseReleaseEvent(QMouseEvent *event)
{
}

void MinecraftWorld::onInitialize()
{
    _entities.clear();

    addEntity(new Player(Vector3(50, 3, -15), Vector3(0.8, 1.5, 0.8)));
    addEntity(new Enemy(Vector3(55, 10, -20), Vector3(1, 2, 1)));
    addEntity(new Enemy(Vector3(60, 10, -5), Vector3(1, 2, 1)));
    addEntity(new Enemy(Vector3(80, 10, -10), Vector3(1, 2, 1)));
    addEntity(new Enemy(Vector3(120, 10, 0), Vector3(1, 2, 1)));
    addEntity(new Enemy(Vector3(0, 32, -10), Vector3(1, 2, 1)));
    addEntity(new Enemy(Vector3(40, 10, 0), Vector3(1, 2, 1)));

    if(m_voxelmanager != NULL)
        delete m_voxelmanager;
    m_voxelmanager = new MinecraftVoxelManager();
    m_voxelmanager->onInitialize( _entities[0]->pos);
}

void MinecraftWorld::restart()
{
    cout<<"restart the game."<<endl;

    onInitialize();
}

bool MinecraftWorld::isPlayerAlive()
{
    return _entities[0]->blood > 0 && _entities[0]->identity == PLAYER;
}
