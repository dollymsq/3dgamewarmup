#ifndef MINECRAFTWORLD_H
#define MINECRAFTWORLD_H
#include "engine/world/world.h"
#include "game/world/player.h"
#include "game/world/enemy.h"
#include "game/world/bullet.h"
#include "game/voxel/minecraftvoxelmanager.h"


class MinecraftWorld : public World
{
public:
    MinecraftWorld();

    virtual void onTick(float seconds, Camera *cam);
    virtual void onDraw(Graphics *g, GLfloat *matrix);

    virtual int onKeyPressEvent(QKeyEvent *event);
    virtual void onKeyReleaseEvent(QKeyEvent *event);

    virtual void onMousePressEvent(QMouseEvent *event);
    virtual void onMouseReleaseEvent(QMouseEvent *event);

    virtual void onInitialize();
    virtual void restart();

    bool isPlayerAlive();
private:
    BlockIntersect cameraIntersect;
    Vector3 look;
//    bool playerisdead;
};

#endif // MINECRAFTWORLD_H
