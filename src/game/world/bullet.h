#ifndef BULLET_H
#define BULLET_H
#include "engine/world/entity.h"

class Bullet : public Entity
{
public:
    Bullet(Vector3 p, Vector3 d);

    virtual void onTick(float seconds);

    virtual void MoveInXZPlane(float t);
    virtual void setDirection(Camera *cam, Vector3 v);

    virtual void onCollide(Entity *e, Vector3 mtv);

};

#endif // BULLET_H
