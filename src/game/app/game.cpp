#include "game.h"

Game::Game()
{
}

void Game::onInitialize()
{
    //initialize two screens
    Screen* scr1 = new IntroScreen();
    Screen* scr2 = new GameScreen();
    scr2->onInitialize();
    m_screenStack.push(scr2);
    m_screenStack.push(scr1);
    m_currentScreen = m_screenStack.top();
}

void Game::onKeyPressEvent(QKeyEvent *event)
{
    int state = m_currentScreen->onKeyPressEvent(event);
    if(state == 0)
    {
        delete m_screenStack.pop();
        m_currentScreen = m_screenStack.top();
    }
    else if(state == 2)
    {
        delete m_screenStack.pop();
        Screen* scr2 = new GameScreen();
        scr2->onInitialize();
        m_screenStack.push(scr2);
        m_currentScreen = m_screenStack.top();
    }
}

