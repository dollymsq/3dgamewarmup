#ifndef Game_H
#define Game_H
#include <qgl.h>
#include "engine/app/application.h"
#include "game/app/introscreen.h"
#include "game/app/gamescreen.h"

class Game : public Application
{
public:
    Game();
    virtual void onInitialize();
    virtual void onKeyPressEvent(QKeyEvent *event);
};

#endif // Game_H
