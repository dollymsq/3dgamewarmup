#include "introscreen.h"

IntroScreen::IntroScreen()
{
}

void IntroScreen::onDraw(Graphics *g, GLfloat *matrix)
{
    g->renderText(160, 160, 18, "Press Space to enter the game!");
}

int IntroScreen::onKeyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space)
        return 0;
}
