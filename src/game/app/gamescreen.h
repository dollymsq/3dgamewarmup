#ifndef GAMESCREEN_H
#define GAMESCREEN_H
#include "engine/app/screen.h"
#include "game/world/minecraftworld.h"

class GameScreen: public Screen
{
public:
    GameScreen();
    ~GameScreen();

    virtual void onTick(float seconds, Camera *cam);
    virtual void onDraw(Graphics *g, GLfloat *matrix);

    virtual void onInitialize();

    virtual void onMouseMoveEvent(int x, int y, Camera *cam);    
    virtual void onMousePressEvent(QMouseEvent *event);
    virtual void onMouseReleaseEvent(QMouseEvent *event);

    virtual int onKeyPressEvent(QKeyEvent *event);
    virtual void onKeyReleaseEvent(QKeyEvent *event);


};

#endif // GAMESCREEN_H
