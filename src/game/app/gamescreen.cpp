#include "gamescreen.h"

GameScreen::GameScreen()
{
    m_world = new MinecraftWorld();
}

GameScreen::~GameScreen()
{
    delete m_world;
}

void GameScreen::onTick(float seconds, Camera *cam)
{
   m_world->onTick(seconds, cam);
}

void GameScreen::onDraw(Graphics *g, GLfloat *matrix)
{
    m_world->onDraw(g, matrix);
    glDisable(GL_DEPTH_TEST);
    g->renderText(10, 40, 12, "Press Shift to change camera mode, Q to shoot, R to restart.");
//    g->drawCross();
}

void GameScreen::onInitialize()
{
    m_world->onInitialize();
}

void GameScreen::onMouseMoveEvent(int x, int y, Camera *cam)
{
    cam->cameraOrient(x, y);
}

void GameScreen::onMousePressEvent(QMouseEvent *event)
{
    m_world->onMousePressEvent(event);
}

void GameScreen::onMouseReleaseEvent(QMouseEvent *event)
{
    m_world->onMouseReleaseEvent(event);
}

int GameScreen::onKeyPressEvent(QKeyEvent *event)
{
    return m_world->onKeyPressEvent(event);
}

void GameScreen::onKeyReleaseEvent(QKeyEvent *event)
{
    m_world->onKeyReleaseEvent(event);
}
