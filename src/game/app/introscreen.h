#ifndef INTROSCREEN_H
#define INTROSCREEN_H
#include "engine/app/screen.h"

class IntroScreen : public Screen
{
public:
    IntroScreen();
    virtual void onTick(float seconds, Camera *cam) {}
    virtual void onDraw(Graphics *g, GLfloat *matrix);

    virtual void onInitialize() {}

    virtual void onMouseMoveEvent(int x, int y, Camera *cam) {}

    virtual void onMousePressEvent(QMouseEvent *event) {}
    virtual void onMouseReleaseEvent(QMouseEvent *event) {}

    virtual int onKeyPressEvent(QKeyEvent *event);
    virtual void onKeyReleaseEvent(QKeyEvent *event) {}
};

#endif // INTROSCREEN_H
