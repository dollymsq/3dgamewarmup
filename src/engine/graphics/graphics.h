#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <qgl.h>
#include <QHash>
#include "QGLWidget"
#include <GL/glu.h>
#include "engine/math/vector.h"

#include <QtOpenGL>
#include <QFile>
#include <QTextStream>
#include <iostream>
#ifndef __APPLE__
extern "C" {
    GLAPI void APIENTRY glBindBuffer (GLenum target, GLuint buffer);
    GLAPI void APIENTRY glGenBuffers (GLsizei n, GLuint *buffers);
    GLAPI void APIENTRY glBufferData (GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
}
#endif

enum Face {YPOS, YNEG, XPOS, XNEG, ZPOS, ZNEG};

class Graphics
{
public:
    Graphics(QGLWidget *v) {  view = v; rowcounter = 0; colcounter = 0;}
    ~Graphics();

    void drawQuad();
    void drawEnityAsCube(Vector3 pos, Vector3 dim);

    void drawYposQuad(Vector2 tl, float wid);
    void drawYnegQuad(Vector2 tl, float wid);
    void drawXposQuad(Vector2 tl, float wid);
    void drawXnegQuad(Vector2 tl, float wid);
    void drawZposQuad(Vector2 tl, float wid);
    void drawZnegQuad(Vector2 tl, float wid);

    void drawCross();
    void drawIntersectionFace(int faceHit);
    void drawIntersectionPoint(Vector3 p);

    void renderText(int x, int y, int fontsize, const QString text);
    void renderVbo(GLuint vbo, int visiblefaces);
    void loadTexture(const QString filename, float r, float c);
    void bindTexture(const QString filename);
    void unbindTexture();

    QHash<QString, GLuint> texHash;

    float screen_width, screen_height;

private:
    QGLWidget *view;
    float rowcounter, colcounter, blocktexwid;

};

#endif // GRAPHICS_H
