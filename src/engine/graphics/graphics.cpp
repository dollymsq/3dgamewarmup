#include "graphics.h"

Graphics::~Graphics()
{
    foreach (GLuint id, texHash)
        glDeleteTextures(1, &id);
}

void Graphics::loadTexture(const QString filename, float r, float c)
{
    rowcounter = r;
    colcounter = c;
    blocktexwid = 1.0f /rowcounter;

    QImage img(filename);
    img = QGLWidget::convertToGLFormat(img);

    GLuint texId = 0;
    glGenTextures(1, &texId);
    glBindTexture(GL_TEXTURE_2D, texId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);//change GL_LINEAR to GL_NEASREST
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width(), img.height(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());

    glBindTexture(GL_TEXTURE_2D, 0);

    texHash[filename] = texId;
}

void Graphics::bindTexture(const QString filename)
{
    GLuint texId = texHash[filename];
    glBindTexture(GL_TEXTURE_2D, texId);
    glEnable(GL_TEXTURE_2D);
}

void Graphics::unbindTexture()
{
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
}


void Graphics::drawQuad()
{
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);	glVertex3f(0,0,0);	//	remember	winding	order!
    glTexCoord2f(1,0);	glVertex3f(1,0,0);
    glTexCoord2f(1,1);	glVertex3f(1,0,1);
    glTexCoord2f(0,1);	glVertex3f(0,0,1);
    glEnd();
}

void Graphics::drawEnityAsCube(Vector3 pos, Vector3 dim)
{
    glPushMatrix();
    glTranslatef(pos.x, pos.y + dim.y/2, pos.z);
    glScalef(dim.x, dim.y, dim.z);
    glTranslatef(-0.5, -0.5, -0.5);

    glBegin(GL_QUADS);
    drawYposQuad(Vector2(0, 1), 1);
    drawYnegQuad(Vector2(0, 1), 1);
    drawXposQuad(Vector2(0, 1), 1);
    drawXnegQuad(Vector2(0, 1), 1);
    drawZposQuad(Vector2(0, 1), 1);
    drawZnegQuad(Vector2(0, 1), 1);

    glEnd();
    glPopMatrix();
}

void Graphics::drawYposQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 0, 1, 0);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 0, 1, 1);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 1, 1, 1);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 1, 1, 0);
}

void Graphics::drawYnegQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 0, 0, 1);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 0, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 1, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 1, 0, 1);
}

void Graphics::drawXposQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 1, 1, 1);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 1, 0, 1);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 1, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 1, 1, 0);
}

void Graphics::drawXnegQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 0, 1, 0);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 0, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 0, 0, 1);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 0, 1, 1);
}

void Graphics::drawZposQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 0, 1, 1);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 0, 0, 1);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 1, 0, 1);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 1, 1, 1);
}

void Graphics::drawZnegQuad(Vector2 tl, float wid)
{
    wid == 1 ? 1 : blocktexwid;

    glTexCoord2f(tl.x, tl.y);               glVertex3f( 1, 1, 0);
    glTexCoord2f(tl.x, tl.y - wid);         glVertex3f( 1, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y - wid);   glVertex3f( 0, 0, 0);
    glTexCoord2f(tl.x + wid, tl.y);         glVertex3f( 0, 1, 0);
}

void Graphics::drawCross()
{
    float centerX = screen_width/2;
    float centerY = screen_height/2;

    glColor3f(0.5, 0.5, 0.5);

    view->renderText(centerX, centerY, "X", QFont("Helvetica", 20));
}

void Graphics::drawIntersectionPoint(Vector3 p)
{
//    GLUquadric* quad = gluNewQuadric();
//    glClear(GL_COLOR_BUFFER_BIT);

//    glColor3f(1,1,1);
//    glPushMatrix();
//    glTranslatef(p.x, p.y,p.z);
//    gluSphere(quad, 0.005, 10, 10);
//    glPopMatrix();

}

void Graphics::drawIntersectionFace(int faceHit)
{
    //    glColor3f(1,1,1);

    //    glBegin(GL_LINE_LOOP);
    //    glVertex3f( 0 - 0.01, 1 + 0.01, 0 - 0.01);
    //    glVertex3f( 0 - 0.01, 1 + 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 1 + 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 1 + 0.01, 0 - 0.01);
    //    glEnd();

    //    glBegin(GL_LINE_LOOP);
    //    glVertex3f( 0 - 0.01, 0 - 0.01, 0 - 0.01);
    //    glVertex3f( 0 - 0.01, 0 - 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 0 - 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 0 - 0.01, 0 - 0.01);
    //    glEnd();

    //    glBegin(GL_LINES);
    //    glVertex3f( 0 - 0.01, 1 + 0.01, 0 - 0.01);
    //    glVertex3f( 0 - 0.01, 0 - 0.01, 0 - 0.01);
    //    glVertex3f( 0 - 0.01, 1 + 0.01, 1 + 0.01);
    //    glVertex3f( 0 - 0.01, 0 - 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 1 + 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 0 - 0.01, 1 + 0.01);
    //    glVertex3f( 1 + 0.01, 1 + 0.01, 0 - 0.01);
    //    glVertex3f( 1 + 0.01, 0 - 0.01, 0 - 0.01);
    //    glEnd();

    glBegin(GL_QUADS);
    switch(faceHit)
    {
    case YPOS:
        drawYposQuad(Vector2(0,1), 1);
        break;
    case YNEG:
        drawYnegQuad(Vector2(0,1), 1);
        break;
    case XPOS:
        drawXposQuad(Vector2(0,1), 1);
        break;
    case XNEG:
        drawXnegQuad(Vector2(0,1), 1);
        break;
    case ZPOS:
        drawZposQuad(Vector2(0,1), 1);
        break;
    case ZNEG:
        drawZnegQuad(Vector2(0,1), 1);
        break;
    default:
        break;
    }

    glEnd();
}

void Graphics::renderText(int x, int y, int fontsize, const QString text)
{
    glColor3f(1,1,1);
    view->renderText(x, y, text, QFont("Helvetica", fontsize));
}

void Graphics::renderVbo(GLuint vbo, int visiblefaces)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 5 * sizeof(float), (void*)0);
    glTexCoordPointer(2, GL_FLOAT, 5 * sizeof(float), (void*) (3 * sizeof(float)));
    glDrawArrays(GL_QUADS, 0, visiblefaces * 4);

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


