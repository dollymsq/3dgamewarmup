#ifndef APPLICATION_H
#define APPLICATION_H
#include "screen.h"

class Application
{
public:
    Application();
    ~Application();

    virtual void onInitialize() = 0;
    virtual void onTick(float seconds, Camera *cam);
    void onResize(int w, int h, Camera *cam);
    void onDraw(Graphics* g, GLfloat *matrix);

    void onMouseMoveEvent(int x, int y, Camera *cam);
    void onMousePressEvent(QMouseEvent *event);
    void onMouseReleaseEvent(QMouseEvent *event);

    virtual void onKeyPressEvent(QKeyEvent *event) = 0;
    void onKeyReleaseEvent(QKeyEvent *event);

protected:
    QStack<Screen *> m_screenStack;
    Screen* m_currentScreen;
};

#endif // APPLICATION_H
