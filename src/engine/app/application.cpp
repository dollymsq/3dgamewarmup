#include "application.h"

Application::Application()
{
}

Application::~Application()
{
    if (!m_screenStack.empty())
        delete m_screenStack.pop();
}

void Application::onTick(float seconds, Camera *cam)
{
    m_currentScreen->onTick(seconds, cam);
}

void Application::onResize(int w, int h, Camera *cam)
{
    glViewport(0,0,w,h);
    m_currentScreen->onResize(w,h,cam);
}

void Application::onDraw(Graphics *g, GLfloat *matrix)
{
    m_currentScreen->onDraw(g, matrix);
}

void Application::onMouseMoveEvent(int x, int y, Camera *cam)
{
    m_currentScreen->onMouseMoveEvent(x, y, cam);
}

void Application::onMousePressEvent(QMouseEvent *event)
{
    m_currentScreen->onMousePressEvent(event);
}

void Application::onMouseReleaseEvent(QMouseEvent *event)
{
    m_currentScreen->onMouseReleaseEvent(event);

}

void Application::onKeyReleaseEvent(QKeyEvent *event)
{
    m_currentScreen->onKeyReleaseEvent(event);
}


