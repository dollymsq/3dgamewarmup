#ifndef SCREEN_H
#define SCREEN_H
#include <QKeyEvent>
#include <QStack>
#include "engine/math/camera.h"
#include "engine/graphics/graphics.h"
#include "engine/world/world.h"

class Screen
{
public:
    Screen() {}
    virtual ~Screen() {}

    virtual void onTick(float seconds, Camera *cam) = 0;
    virtual void onDraw(Graphics *g, GLfloat *matrix) = 0;

    virtual void onInitialize() = 0;
    virtual void onResize(float w, float h, Camera *cam);


    virtual void onMousePressEvent(QMouseEvent *event) = 0;
    virtual void onMouseReleaseEvent(QMouseEvent *event) = 0;
    virtual void onMouseMoveEvent(int x, int y, Camera *cam) = 0;
    virtual int onKeyPressEvent(QKeyEvent *event) = 0;
    virtual void onKeyReleaseEvent(QKeyEvent *event) = 0;

    World* m_world;
};

#endif // SCREEN_H
