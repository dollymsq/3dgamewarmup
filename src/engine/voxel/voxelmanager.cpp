#include "voxelmanager.h"

VoxelManager::VoxelManager()
{
    chunkTable.clear();
}

VoxelManager::~VoxelManager()
{
    foreach(Chunk *chk, chunkTable)
        delete chk;
}

float VoxelManager::Noise(int x, int y)
{
    uint n = x + y * 57;
    n = (n << 13) ^ n;
    return (1.0 - float((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float VoxelManager::smoothNoise(float x, float y)
{
    float corners = ( Noise(x-1, y-1)+Noise(x+1, y-1)+Noise(x-1, y+1)+Noise(x+1, y+1) ) / 16;
    float sides   = ( Noise(x-1, y)  +Noise(x+1, y)  +Noise(x, y-1)  +Noise(x, y+1) ) /  8;
    float center  =  Noise(x, y) / 4;
    return corners + sides + center;
}

float VoxelManager::interpolateNoise(float x, float y)
{
    int integer_X    = int(x);
    float fractional_X = x - integer_X;

    int integer_Y    = int(y);
    float fractional_Y = y - integer_Y;

    float v1 = smoothNoise(integer_X,     integer_Y);
    float v2 = smoothNoise(integer_X + 1, integer_Y);
    float v3 = smoothNoise(integer_X,     integer_Y + 1);
    float v4 = smoothNoise(integer_X + 1, integer_Y + 1);

    float i1 = cos_interpolate(v1 , v2 , fractional_X);
    float i2 = cos_interpolate(v3 , v4 , fractional_X);

    return cos_interpolate(i1 , i2 , fractional_Y);
}

float VoxelManager::cos_interpolate(float a, float b, float x)
{
    float ft =  x * M_PI;
    float f = (1- cos(ft)) * 0.5f;

    return a * (1 - f) + b * f;
}

float VoxelManager::valueNoise2D(float x, float y)
{
    x+= 2000;
    y+= 2000;
    float total = 0;
    float p = 0.98; //persistence
    int n = 4;
    for( int i = 0 ; i < n; i ++)
    {
        float freq = pow(2, i);
        float ampl = pow(p, i);

        total += interpolateNoise(x * freq, y * freq) * ampl;
    }

    return total;
}

BlockIntersect VoxelManager::rayCasting(Vector3 p, Vector3 d)
{
    BlockIntersect vint;
    // +1 or -1 depending on direction the player is moving
    const int stepX = d.x > 0 ? 1 : -1;
    const int stepY = d.y > 0 ? 1 : -1;
    const int stepZ = d.z > 0 ? 1 : -1;

    // t delta to span an entire cell
    const float tDeltaX = fabs(d.x) > EPSILON ? fabs(1.0/d.x) : INT_MAX;
    const float tDeltaY = fabs(d.y) > EPSILON ? fabs(1.0/d.y) : INT_MAX;
    const float tDeltaZ = fabs(d.z) > EPSILON ? fabs(1.0/d.z) : INT_MAX;

    // integer block coordinates
    int X =  d.x > 0 ? ceil(p.x) - 1 : floor(p.x);
    int Y =  d.y > 0 ? ceil(p.y) - 1 : floor(p.y);
    int Z =  d.z > 0 ? ceil(p.z) - 1 : floor(p.z);

    // t value to next integer boundary
    //handle divide by zero problem
    float tMaxX = tDeltaX;
    float tMaxY = tDeltaY;
    float tMaxZ = tDeltaZ;

    tMaxX = (d.x > 0 ? (ceil(p.x) - p.x)/d.x : (floor(p.x) - p.x)/d.x);
    tMaxY = (d.y > 0 ? (ceil(p.y) - p.y)/d.y : (floor(p.y) - p.y)/d.y);
    tMaxZ = (d.z > 0 ? (ceil(p.z) - p.z)/d.z : (floor(p.z) - p.z)/d.z);

    //to be refined
    char direction = 0; // should be equal to x, y or z,
    while(1)
    {
        if(tMaxX >= INT_MAX && tMaxY >= INT_MAX && tMaxZ >= INT_MAX)
        {
            vint.exist = false;
            break;
        }
        QPair<QPair<int, int>, int> chkidx;
        int blkidx;
        getIndexByWorldCoor(X, Y, Z, chkidx, blkidx);
        //to modify after index chunk method changes
        if(chunkTable.contains(chkidx))
        {
            if(chunkTable[chkidx]->_blocks[blkidx].type != 0)
            {
                vint.exist = true;
                vint.selectedBlkCoor = Vector3(X, Y, Z);
                switch(direction)
                {
                case 'x':
                    if(stepX == 1)
                        vint.faceHit = XNEG; // to add a block at X-1
                    else
                        vint.faceHit = XPOS;
                    break;
                case 'y':
                    if(stepY == 1)
                        vint.faceHit = YNEG; // to add a block at Y-1
                    else
                        vint.faceHit = YPOS;
                    break;
                case 'z':
                    if(stepZ == 1)
                        vint.faceHit = ZNEG; // to add a block at Z-1
                    else
                        vint.faceHit = ZPOS;
                    break;
                default:
                    break;
                }
                break;
            }
        }
        else
            break;
        if (tMaxX < tMaxY && tMaxX < tMaxZ )
        {
            tMaxX += tDeltaX;
            X += stepX;
            direction = 'x';
        }
        else if(tMaxY < tMaxZ)
        {
            tMaxY += tDeltaY;
            Y += stepY;
            direction = 'y';
        }
        else
        {
            tMaxZ += tDeltaZ;
            Z += stepZ;
            direction = 'z';
        }
    }

    return vint;
}

void VoxelManager::onDraw(Graphics *g, GLfloat *matrix, const BlockIntersect &v)
{
    g->bindTexture(":/block/terrain.png");
    QPair<QPair<int, int>, int> centeridx;
    int blidx;
    getIndexByWorldCoor(playerPos.x, playerPos.y, playerPos.z, centeridx, blidx);
    for(int xn = centeridx.first.first - 2; xn <= centeridx.first.first + 2; xn ++)
        for(int zn = centeridx.second - 2; zn <= centeridx.second + 2; zn ++)
            for(int yn = centeridx.first.second - 2; yn <= centeridx.first.second + 2; yn ++)
            {
                QPair<QPair<int, int>, int> chkidx = qMakePair(qMakePair(xn, yn), zn);
                if(chunkTable.contains(chkidx))
                {
                    glPushMatrix();
                    glTranslatef(xn*CHUNK_SIZE, yn*CHUNK_SIZE, zn*CHUNK_SIZE);
                    if(isWithinFrustum(xn, yn, zn, matrix))
                    {
                        chunkTable[chkidx]->render(g, texTable);
                    }
                    glPopMatrix();
                }
            }
    g->unbindTexture();

    //render the intersected block with eye ray
    if(v.exist)
    {
        g->bindTexture(":/block/intersectface.png");
        glPushMatrix();
        glTranslatef(v.selectedBlkCoor.x + 0.5, v.selectedBlkCoor.y + 0.5, v.selectedBlkCoor.z + 0.5);
        glScalef(1.01, 1.01, 1.01);
        glTranslatef(-0.5, -0.5, -0.5);

        g->drawIntersectionFace(v.faceHit);

        glPopMatrix();
        g->unbindTexture();
    }
}

Vector3 VoxelManager::collisionDetect(Vector3 prevpos, Entity *e, Vector3 nextposition)
{
    Vector3 afterpos;
    afterpos.x = collisionDetectX(prevpos, e->dim, nextposition);
    afterpos.z = collisionDetectZ(prevpos, e->dim, nextposition);
    afterpos.y = collisionDetectY(prevpos, e, nextposition.y);
    return afterpos;
}

float VoxelManager::collisionDetectX(Vector3 prevpos, Vector3 dim, Vector3 nextposition)
{
    //for each axis, there are two stripes to test
    QPair<QPair<int, int>, int> chidx;
    int blidx;

    float t1 = (nextposition - prevpos).x;
    float tmin = t1;

    //x direction
    Vector3 start1 = Vector3(floor(prevpos.x - dim.x/2), floor(prevpos.y), floor(prevpos.z - dim.z/2));


    for(int j = start1.z; j <= ceil(prevpos.z + dim.z/2) - 1; j++) // two stripes
    {
        if(t1 > 0)
        {
            for(int i = ceil(prevpos.x + dim.x/2); i <= ceil(nextposition.x + dim.x/2); i ++)
            {
                for( int k = start1.y; k <= ceil(prevpos.y + dim.y) - 1; k ++)
                {
                    getIndexByWorldCoor(i, k, j, chidx, blidx);

                    if(!chunkTable.contains(chidx))
                    {
//                        continue;
                        chunkTable[chidx] = generateAChunk(chidx);
                        if(chunkToLoad.contains(chidx))
                            chunkToLoad.removeOne(chidx);
                    }
                    if(chunkTable[chidx]->_blocks[blidx].type!= 0 &&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER) // y direction plus one
                    {
                        t1 = i - dim.x/2 - EPSILON - prevpos.x;
                        tmin = t1 < tmin? t1 : tmin;
                    }
                }
            }
        }
        else if(t1 < 0)
        {
            for(int i = floor(prevpos.x - dim.x/2)-1; i >= floor(nextposition.x - dim.x/2); i--)
            {
                for( int k = start1.y; k <= ceil(prevpos.y + dim.y) - 1; k ++)
                {
                    getIndexByWorldCoor(i, k, j, chidx, blidx);

                    if(!chunkTable.contains(chidx))
                        continue;
                    else if(chunkTable[chidx]->_blocks[blidx].type!= 0 &&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER) // y direction plus one
                    {
                        t1 = (i + dim.x/2 + 1) - prevpos.x + EPSILON;
                        tmin = t1 > tmin? t1 : tmin;
                    }
                }
            }
        }
    }
    return prevpos.x + tmin;
}

float VoxelManager::collisionDetectZ(Vector3 prevpos, Vector3 dim, Vector3 nextposition)
{
    //for each axis, there are stripes to test
    QPair<QPair<int, int>, int> chidx;
    int blidx;

    float t1 = (nextposition - prevpos).z;
    float tmin = t1;

    //z direction
    Vector3 start1 = Vector3(floor(prevpos.x - dim.x/2), floor(prevpos.y),floor(prevpos.z - dim.z/2));

    for(int j = start1.x; j <= ceil(prevpos.x + dim.x/2) - 1; j++) //
    {
        if(t1 > 0)
        {
            for(int i = ceil(prevpos.z + dim.z/2); i <= ceil(nextposition.z + dim.z/2); i++)
            {
                for( int k = start1.y; k <= ceil(prevpos.y + dim.y) - 1; k ++)
                {
                    getIndexByWorldCoor(j, k, i, chidx, blidx);


                    if(!chunkTable.contains(chidx))
                    {
//                        continue;
                        chunkTable[chidx] = generateAChunk(chidx);
                        if(chunkToLoad.contains(chidx))
                            chunkToLoad.removeOne(chidx);
                    }
                    if(chunkTable[chidx]->_blocks[blidx].type!= 0 &&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER) // y direction plus one
                    {
                        t1 = i - dim.z/2 - EPSILON - prevpos.z;
                        tmin = t1 < tmin? t1:tmin;
                    }
                }
            }
        }
        else if(t1 < 0)
        {
            for(int i= floor(prevpos.z - dim.z/2)-1; i >= floor(nextposition.z - dim.z/2); i--)
            {
                getIndexByWorldCoor(j, start1.y, i, chidx, blidx);
                for( int k = start1.y; k <= ceil(prevpos.y + dim.y) - 1; k ++)
                {
                    getIndexByWorldCoor(j, k, i, chidx, blidx);

                    if(!chunkTable.contains(chidx))
                        continue;
                    else if(chunkTable[chidx]->_blocks[blidx].type!= 0 &&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER) // y direction plus one
                    {
                        t1 = (i + dim.z/2 + 1) - prevpos.z + EPSILON;
                        tmin = t1 > tmin? t1:tmin;
                    }
                }
            }
        }
    }

    return prevpos.z + tmin;
}

float VoxelManager::collisionDetectY(Vector3 prevpos, Entity *player, float nextposy)
{
    QPair<QPair<int, int>, int> chidx;
    int blidx;
    float radiusx = player->dim.x/2;
    float radiusz = player->dim.z/2;
    float height = player->dim.y;
    Vector3 stablock(floor(prevpos.x - radiusx), floor(prevpos.y), floor(prevpos.z - radiusx));
    Vector3 endblock(ceil(prevpos.x + radiusz)-1, floor(prevpos.y + height), ceil(prevpos.z + radiusz)-1);
    float htmax = nextposy, ht1;
    //check up to x*z pillars
    if(nextposy >= prevpos.y) //up process, check the top
    {
        for(int x = stablock.x; x<= endblock.x; x++)
            for(int z = stablock.z; z<= endblock.z; z++)
            {
                for( int y = endblock.y; y <= floor(nextposy + height); y ++)
                {
                    getIndexByWorldCoor(x, y, z,chidx,blidx);
                    if(!chunkTable.contains(chidx))
                    {
//                        continue;
                        chunkTable[chidx] = generateAChunk(chidx);
                        if(chunkToLoad.contains(chidx))
                            chunkToLoad.removeOne(chidx);
                    }
                    if(chunkTable[chidx]->_blocks[blidx].type != 0&&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER)
                    {
                        ht1 = y - height - EPSILON;
                        htmax = ht1 < htmax? ht1 : htmax;
                    }
                }
            }
        if(htmax < nextposy)
        {
            player->vup = 0;
            player->ybeforejump = htmax;
            player->delta = 0;
        }
    }
    else // falling down, check bottom
    {
        for(int x = stablock.x; x <= endblock.x; x ++)
            for(int z = stablock.z; z <= endblock.z; z ++)
            {
                for(int y = stablock.y; y >= floor(nextposy); y --)
                {
                    getIndexByWorldCoor(x, y, z, chidx, blidx);
                    if(!chunkTable.contains(chidx))
                        continue;
                    else if(chunkTable[chidx]->_blocks[blidx].type != 0 &&
                            chunkTable[chidx]->_blocks[blidx].type!= WATER)
                    {
                        ht1 = y + 1 + EPSILON;
                        htmax = ht1 > htmax? ht1: htmax;
                    }
                }
            }
        if(htmax > nextposy)
        {
            player->vup = 0; //reset for next time
            player->inair = false;
            player->ybeforejump = htmax;
            player->delta = 0;
        }
    }
    return htmax;
}

bool VoxelManager::isWithinFrustum(int xn, int yn, int zn, GLfloat *matrix)
{
    //handle the matrix for view frustum culling
    Vector4 r0= Vector4(matrix[0], matrix[4], matrix[8], matrix[12]);
    Vector4 r1= Vector4(matrix[1], matrix[5], matrix[9], matrix[13]);
    Vector4 r2= Vector4(matrix[2], matrix[6], matrix[10], matrix[14]);
    Vector4 r3= Vector4(matrix[3], matrix[7], matrix[11], matrix[15]);

    //check if within view frustum, the 8 corners of chunk
    Vector4 v1 = Vector4(xn*CHUNK_SIZE, yn*CHUNK_SIZE, zn*CHUNK_SIZE, 1);
    Vector4 v2 = Vector4(xn*CHUNK_SIZE, yn*CHUNK_SIZE, (zn+1)*CHUNK_SIZE, 1);
    Vector4 v3 = Vector4(xn*CHUNK_SIZE, (yn+1)*CHUNK_SIZE, zn*CHUNK_SIZE, 1);
    Vector4 v4 = Vector4(xn*CHUNK_SIZE, (yn+1)*CHUNK_SIZE, (zn+1)*CHUNK_SIZE, 1);
    Vector4 v5 = Vector4((xn+1)*CHUNK_SIZE, yn*CHUNK_SIZE, zn*CHUNK_SIZE, 1);
    Vector4 v6 = Vector4((xn+1)*CHUNK_SIZE, yn*CHUNK_SIZE, (zn+1)*CHUNK_SIZE, 1);
    Vector4 v7 = Vector4((xn+1)*CHUNK_SIZE, (yn+1)*CHUNK_SIZE, zn*CHUNK_SIZE, 1);
    Vector4 v8 = Vector4((xn+1)*CHUNK_SIZE, (yn+1)*CHUNK_SIZE, (zn+1)*CHUNK_SIZE, 1);

    //-x plane
    if(v1.dot(r3-r0)<0 && v2.dot(r3-r0)<0 && v3.dot(r3-r0)<0 && v4.dot(r3-r0)<0
            && v5.dot(r3-r0)<0 && v6.dot(r3-r0)<0 && v7.dot(r3-r0)<0 && v8.dot(r3-r0)<0)
        return 0;
    //-y plane
    if(v1.dot(r3-r1)<0 && v2.dot(r3-r1)<0 && v3.dot(r3-r1)<0 && v4.dot(r3-r1)<0
            && v5.dot(r3-r1)<0 && v6.dot(r3-r1)<0 && v7.dot(r3-r1)<0 && v8.dot(r3-r1)<0)
        return 0;
    //-z
    if(v1.dot(r3-r2)<0 && v2.dot(r3-r2)<0 && v3.dot(r3-r2)<0 && v4.dot(r3-r2)<0
            && v5.dot(r3-r2)<0 && v6.dot(r3-r2)<0 && v7.dot(r3-r2)<0 && v8.dot(r3-r2)<0)
        return 0;

    //+x plane
    if(v1.dot(r3+r0)<0 && v2.dot(r3+r0)<0 && v3.dot(r3+r0)<0 && v4.dot(r3+r0)<0
            && v5.dot(r3+r0)<0 && v6.dot(r3+r0)<0 && v7.dot(r3+r0)<0 && v8.dot(r3+r0)<0)
        return 0;
    //+y plane
    if(v1.dot(r3+r1)<0 && v2.dot(r3+r1)<0 && v3.dot(r3+r1)<0 && v4.dot(r3+r1)<0
            && v5.dot(r3+r1)<0 && v6.dot(r3+r1)<0 && v7.dot(r3+r1)<0 && v8.dot(r3+r1)<0)
        return 0;
    //+z
    if(v1.dot(r3+r2)<0 && v2.dot(r3+r2)<0 && v3.dot(r3+r2)<0 && v4.dot(r3+r2)<0
            && v5.dot(r3+r2)<0 && v6.dot(r3+r2)<0 && v7.dot(r3+r2)<0 && v8.dot(r3+r2)<0)
        return 0;

    return 1;
}

void VoxelManager::onInitialize(Vector3 p)
{
    playerPos = p;

    cTabNumLimit = 150;
    generateTexTable();

    QPair<QPair<int, int>, int> chidx;
    int blidx;
    getIndexByWorldCoor(playerPos.x, playerPos.y, playerPos.z, chidx, blidx);

    int chx = chidx.first.first;
    int chy = chidx.first.second;
    int chz = chidx.second;

    for(int x = chx - 2; x <= chx + 2; x ++)
        for(int z = chz - 2; z <= chz + 2; z ++)
            for(int y = chy - 2; y <= chy + 2; y ++)
            {
                chunkTable[qMakePair(qMakePair(x, y), z)] = generateAChunk(qMakePair(qMakePair(x, y), z));
            }
}

void VoxelManager::handleChunksToLoad()
{
    //calculate which chunks to load, add them to queue
    QPair<QPair<int, int>, int> playerchkidx;
    int blidx;
    getIndexByWorldCoor(playerPos.x, playerPos.y, playerPos.z, playerchkidx, blidx);

    int chx = playerchkidx.first.first;
    int chy = playerchkidx.first.second;
    int chz = playerchkidx.second;

    for(int x = chx - 2; x <= chx + 2; x ++)
        for(int z = chz - 2; z <= chz + 2; z ++)
            for(int y = chy - 2; y <= chy + 2; y ++)
            {
                if(!chunkTable.contains(qMakePair(qMakePair(x, y), z))
                        && !chunkToLoad.contains(qMakePair(qMakePair(x, y), z)))
                    chunkToLoad.enqueue(qMakePair(qMakePair(x, y), z));
            }

//    //refine the list also
//    for(int i = 0; i < chunkToLoad.size(); i ++)
//    {
//        if(chunkToLoad[i].first.first < chx - 2 || chunkToLoad[i].first.first > chx + 2
//                || chunkToLoad[i].first.second < chy -2 || chunkToLoad[i].first.second > chy + 2
//                || chunkToLoad[i].second < chz - 2 || chunkToLoad[i].second > chz + 2)
//        {
//            chunkToLoad.removeAt(i);
//        }
//    }

    //load one chunk from queue's head
    if(!chunkToLoad.empty())
    {
        QPair<QPair<int, int>, int> chidx = chunkToLoad.dequeue();
        while(chidx.first.first < chx - 2 || chidx.first.first > chx + 2
              || chidx.first.second < chy -2 || chidx.first.second > chy + 2
              || chidx.second < chz - 2 || chidx.second > chz + 2)
            chidx = chunkToLoad.dequeue();
        chunkTable[chidx] = generateAChunk(chidx);
    }

    //drop a chunk if reached the table number limit
    QHash<QPair<QPair<int, int>, int>, Chunk*>::iterator it;

//    if(chunkTable.size() > cTabNumLimit)
    {
        for(it = chunkTable.begin(); it != chunkTable.end(); it ++)
        {
            QPair<QPair<int, int>, int> idx = it.key();

            if(idx.first.first < chx - 2 || idx.first.first > chx + 2
                    || idx.first.second < chy -2 || idx.first.second > chy + 2
                    || idx.second < chz - 2 || idx.second > chz + 2)
            {
                delete chunkTable[idx];
                chunkTable.remove(idx);
                break;
            }
        }
    }
}

void VoxelManager::addOneBlock(Vector3 v, int blktype)
{
    QPair<QPair<int, int>, int> chidx;
    int blidx;
    getIndexByWorldCoor(v.x, v.y, v.z, chidx, blidx);

    chunkTable[chidx]->_blocks[blidx].type = blktype;
    Vector3 blkcoo = getBlockCoorByWorldCoor(v.x, v.y, v.z);

    chunkTable[chidx]->setBlockTPProperty(blkcoo.x, blkcoo.y, blkcoo.z);
    chunkTable[chidx]->setBlockFaceProperty(blkcoo.x, blkcoo.y, blkcoo.z);

    chunkTable[chidx]->resetVbo(texTable);
}

void VoxelManager::removeOneBlock(Vector3 v)
{
    QPair<QPair<int, int>, int> chidx;
    int blidx;
    getIndexByWorldCoor(v.x, v.y, v.z, chidx, blidx);

    chunkTable[chidx]->_blocks[blidx].type = 0;
    chunkTable[chidx]->_blocks[blidx].property = 0;

    Vector3 blkcoo = getBlockCoorByWorldCoor(v.x, v.y, v.z);
    chunkTable[chidx]->resetNeighborProperty(blkcoo.x, blkcoo.y, blkcoo.z);
    chunkTable[chidx]->resetVbo(texTable);
}
