#ifndef VOXELMANAGER_H
#define VOXELMANAGER_H
#include "engine/voxel/chunk.h"
#include "engine/graphics/graphics.h"
#include "engine/world/entity.h"
#include <QHash>
#include <QQueue>
#include <QPair>

struct BlockIntersect
{
    bool exist;
    Vector3 selectedBlkCoor;
    int faceHit;
    BlockIntersect() { selectedBlkCoor = Vector3(0,0,0); faceHit = YPOS;}
};


class VoxelManager
{
public:
    VoxelManager();
    ~VoxelManager();

    float Noise(int x, int y);
    float smoothNoise(float x, float y);
    float interpolateNoise(float x, float y);
    float cos_interpolate(float a, float b, float x);
    float valueNoise2D(float x, float y);
    BlockIntersect rayCasting(Vector3 p, Vector3 d);

    QHash<QPair<QPair<int, int>, int>, Chunk*> chunkTable;
    BlockTexCoor *texTable; // map from char(block type) to texcoor

    virtual void onDraw(Graphics *g, GLfloat *matrix, const BlockIntersect &v);
    virtual void generateTexTable() = 0;
    virtual Chunk* generateAChunk( QPair<QPair<int, int>, int> chkidx) = 0;

    virtual bool isWithinFrustum(int xn, int yn, int zn, GLfloat *matrix);

    Vector3 collisionDetect(Vector3 prevpos, Entity *e, Vector3 nextposition);
    float collisionDetectX(Vector3 prevpos, Vector3 dim, Vector3 nextposition);
    float collisionDetectZ(Vector3 prevpos, Vector3 dim, Vector3 nextposition);
    float collisionDetectY(Vector3 prevpos, Entity *player, float nextposy);

    void onInitialize(Vector3 p);
    void handleChunksToLoad();

    void addOneBlock(Vector3 v, int blktype);
    void removeOneBlock(Vector3 v);

    Vector3 playerPos;

protected:
    QQueue<QPair<QPair<int, int>, int> > chunkToLoad;
    int cTabNumLimit;
};

#endif // VOXELMANAGER_H
