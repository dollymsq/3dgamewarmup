#include "chunk.h"
#include <bitset>

Chunk::Chunk()
{
    _blocks = new Block[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    blocktexwid = 1.0f / 16;
}

Chunk::~Chunk()
{
    delete[] _blocks;
}

void Chunk::render(Graphics *g, BlockTexCoor *textab)
{
    g->renderVbo(m_vbo, visiblefaces);
}

void Chunk::renderBlock(Graphics *g, const Block &b, BlockTexCoor *textab)
{
    BlockTexCoor texc = textab[b.type];

     //if visible
    if((b.property & 32) != 0)
        g->drawYposQuad(texc.top, 0);

    if((b.property & 16) != 0)
        g->drawYnegQuad(texc.bot, 0);

    if((b.property & 8) != 0)
        g->drawXposQuad(texc.bdy, 0);

    if((b.property & 4) != 0)
        g->drawXnegQuad(texc.bdy, 0);

    if((b.property & 2) != 0)
        g->drawZposQuad(texc.bdy, 0);

    if((b.property & 1) != 0)
        g->drawZnegQuad(texc.bdy, 0);
}

void Chunk::setBlockTPProperty(int x, int y, int z)
{
    int blkidx = getBlockIdx(x, y, z);
    //first two bits
    switch(_blocks[blkidx].type)
    {
    case GRASS:
        _blocks[blkidx].property = 0;
        break;
    case DIRT:
        _blocks[blkidx].property = 0;
        break;
    case STONE:
        _blocks[blkidx].property = 0;
        break;
    case SNOWONGRASS:
        _blocks[blkidx].property = 0;
        break;
    case WATER:
        _blocks[blkidx].property = 0b11000000;  //transparent and passable
        break;
    }
}

void Chunk::setBlockFaceProperty(int x, int y, int z)
{
    if(_blocks[getBlockIdx(x, y, z)].type == 0)
        return;

    char p = _blocks[getBlockIdx(x, y, z)].property;

//    if( _blocks[getBlockIdx(x-1, y, z)].type == WATER && _blocks[getBlockIdx(x, y, z)].type == GRASS)
//    {
//        cout<<x-1<< ", "<< y << ", "<< z<<endl;
//        cout<<"type: "<< std::bitset<8>(_blocks[getBlockIdx(x-1, y, z)].type);
//        cout<<" prop: "<< std::bitset<8>(_blocks[getBlockIdx(x-1, y, z)].property) << endl;
//    }

    //6 faces when to draw:
    //if the face is at boundary
    //or if its neighbor doesn't exists
    //or if its neighbor is transparent and itself isn't transparent

    //ypos (top)
    if(y == CHUNK_SIZE - 1 || _blocks[getBlockIdx(x, y+1, z)].type == 0
            ||( ((_blocks[getBlockIdx(x, y+1, z)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128) == 0)))
        p |= 32;
    if(y == 0 || _blocks[getBlockIdx(x, y-1, z)].type == 0
            ||( ((_blocks[getBlockIdx(x, y-1, z)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128) == 0)))
        p |= 16;

    if(x == CHUNK_SIZE - 1 || _blocks[getBlockIdx(x+1, y, z)].type == 0
            ||( ((_blocks[getBlockIdx(x+1, y, z)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128) == 0)))
        p |= 8;
    if(x == 0 || _blocks[getBlockIdx(x-1, y, z)].type == 0
            ||( ((_blocks[getBlockIdx(x-1, y, z)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128 )== 0)))
        p |= 4;

    if(z == CHUNK_SIZE - 1 ||  _blocks[getBlockIdx(x, y, z+1)].type == 0
            ||( ((_blocks[getBlockIdx(x, y, z+1)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128) == 0)))
        p |= 2;
    if(z == 0 || _blocks[getBlockIdx(x, y, z-1)].type == 0
            ||( ((_blocks[getBlockIdx(x, y, z-1)].property & 128) != 0)
                && ((_blocks[getBlockIdx(x, y, z)].property & 128) == 0)))
        p |= 1;

    _blocks[getBlockIdx(x, y, z)].property = p;

}

void Chunk::initVbo(BlockTexCoor *textab)
{
    float *vbobuffer = new float[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * 6 * 4 * 5];
    int idx = 0;

    visiblefaces = 0;
    for(int  z= 0; z< CHUNK_SIZE; z++)
        for(int x = 0; x< CHUNK_SIZE; x++)
            for(int y = 0; y< CHUNK_SIZE; y++)
            {
                idx = getBlockIdx(x,y,z);
                Block b = _blocks[idx];
                BlockTexCoor texc = textab[b.type];

                if(b.type != 0)
                {
                    if((b.property & 32) != 0)
                    {
                        assignYPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.top);
                        visiblefaces++;
                    }

                    if((b.property & 16) != 0)
                    {
                        assignYNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bot);
                        visiblefaces++;
                    }

                    if((b.property & 8) != 0)
                    {
                        assignXPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }

                    if((b.property & 4) != 0)
                    {
                        assignXNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);

                        visiblefaces++;
                    }

                    if((b.property & 2) != 0)
                    {
                        assignZPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }

                    if((b.property & 1) != 0)
                    {
                        assignZNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }
                }
            }

    m_vbo  = 0;
    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, 20 * visiblefaces * sizeof(float), vbobuffer, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    delete[] vbobuffer;
}

void Chunk::resetNeighborProperty(int x, int y, int z)
{
    setBlockFaceProperty(x, y + 1, z);
    setBlockFaceProperty(x, y - 1, z);
    setBlockFaceProperty(x + 1, y, z);
    setBlockFaceProperty(x - 1, y, z);
    setBlockFaceProperty(x, y, z + 1);
    setBlockFaceProperty(x, y, z - 1);
}

void Chunk::resetVbo(BlockTexCoor *textab)
{
    float *vbobuffer = new float[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * 6 * 4 * 5];
    int idx = 0;

    visiblefaces = 0;
    for(int  z= 0; z< CHUNK_SIZE; z++)
        for(int x = 0; x< CHUNK_SIZE; x++)
            for(int y = 0; y< CHUNK_SIZE; y++)
            {
                idx = getBlockIdx(x,y,z);
                Block b = _blocks[idx];
                BlockTexCoor texc = textab[b.type];

                if(b.type != 0)
                {
                    if((b.property & 32) != 0)
                    {
                        assignYPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.top);
                        visiblefaces++;
                    }

                    if((b.property & 16) != 0)
                    {
                        assignYNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bot);
                        visiblefaces++;
                    }

                    if((b.property & 8) != 0)
                    {
                        assignXPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }

                    if((b.property & 4) != 0)
                    {
                        assignXNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);

                        visiblefaces++;
                    }

                    if((b.property & 2) != 0)
                    {
                        assignZPosVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }

                    if((b.property & 1) != 0)
                    {
                        assignZNegVbo(&vbobuffer[20 * visiblefaces], x, y, z, texc.bdy);
                        visiblefaces++;
                    }
                }
            }

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, 20 * visiblefaces * sizeof(float), vbobuffer, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    delete[] vbobuffer;
}

void Chunk::assignOneVertex(float *data, float x, float y, float z, float u, float v)
{
    *data = x;
    *(data+1) = y;
    *(data+2) = z;
    *(data+3) = u;
    *(data+4) = v;
}

void Chunk::assignYPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, x, 1 + y, z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, x, 1 + y, 1 + z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, 1 + x, 1 + y, 1 + z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, 1 + x, 1 + y, z,
                    tl.x + blocktexwid, tl.y);
}

void Chunk::assignYNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, x, y, 1 + z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, x, y, z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, 1 + x,  y,  z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, 1 + x, y, 1 + z,
                    tl.x + blocktexwid, tl.y);
}

void Chunk::assignXPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, 1 + x, 1 + y, 1 + z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, 1 + x, y, 1 + z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, 1 + x, y, z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, 1 + x, 1 + y, z,
                    tl.x + blocktexwid, tl.y);
}

void Chunk::assignXNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, x, 1 + y, z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, x, y, z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, x, y, 1 + z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, x, 1 + y, 1 + z,
                    tl.x + blocktexwid, tl.y);
}

void Chunk::assignZPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, x, 1 + y, 1 + z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, x, y, 1 + z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, 1 + x, y, 1 + z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, 1 + x, 1 + y, 1 + z,
                    tl.x + blocktexwid, tl.y);
}

void Chunk::assignZNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl)
{
    assignOneVertex(vbobuffer, 1 + x, 1 + y, z,
                    tl.x, tl.y);
    assignOneVertex(vbobuffer + 5, 1 + x, y, z,
                    tl.x, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 10, x, y, z,
                    tl.x + blocktexwid, tl.y - blocktexwid);
    assignOneVertex(vbobuffer + 15, x, 1 + y, z,
                    tl.x + blocktexwid, tl.y);
}
