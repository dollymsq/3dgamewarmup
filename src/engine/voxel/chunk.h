#ifndef CHUNK_H
#define CHUNK_H
#include "engine/math/vector.h"
#include "engine/graphics/graphics.h"

#define CHUNK_SIZE 32

#define xHalfNumOfChunk 5 //y,x,z low->high
#define yHalfNumOfChunk 1
#define zHalfNumOfChunk 5

struct Block
{
    char property; // highest bit as transparency, then passability, faces to render(6 bits) y, x, z
    char type;
}__attribute((packed));

struct BlockTexCoor
{
    Vector2 top;
    Vector2 bot;
    Vector2 bdy;
};

enum BlockType
{
    GRASS       = 1,
    DIRT        = 2,
    WATER        = 3,
    STONE       = 4,
    SNOWONGRASS   = 5
};

class Chunk
{
public:
    Chunk();
    ~Chunk();

    virtual void render(Graphics *g, BlockTexCoor *textab);
    virtual void renderBlock(Graphics *g, const Block &b, BlockTexCoor *textab);

    void setBlockTPProperty(int x, int y, int z);
    void setBlockFaceProperty(int x, int y, int z);

    Block *_blocks;

    void initVbo(BlockTexCoor *textab);
    void resetVbo(BlockTexCoor *textab);

    void resetNeighborProperty(int x, int y, int z);
private:
    GLuint m_vbo;
    int visiblefaces;
    float blocktexwid;

    void assignOneVertex(float *data, float x, float y, float z, float u, float v);
    void assignYPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
    void assignYNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
    void assignXPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
    void assignXNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
    void assignZPosVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
    void assignZNegVbo(float *vbobuffer, int x, int y, int z, Vector2 tl);
};

inline int getBlockIdx(int x, int y, int z)
{
    return z * CHUNK_SIZE * CHUNK_SIZE + x * CHUNK_SIZE + y;
}

inline Vector3 getBlockCoorByWorldCoor(int x, int y, int z)
{
    int blx = x % CHUNK_SIZE;
    int bly = y % CHUNK_SIZE;
    int blz = z % CHUNK_SIZE;

    if(blx < 0)    {
        blx += CHUNK_SIZE;
    }
    if(bly < 0)    {
        bly += CHUNK_SIZE;
    }
    if(blz < 0)    {
        blz += CHUNK_SIZE;
    }

    return Vector3(blx, bly, blz);
}

inline int getChunkIdx(int x, int y, int z)
{
    return z * yHalfNumOfChunk * xHalfNumOfChunk * 4 + x * yHalfNumOfChunk * 2 + y;
}

inline void getIndexByWorldCoor(int x, int y, int z, QPair<QPair<int, int>, int> &chidx, int &blidx)
{
    int chx = x / CHUNK_SIZE;
    int chy = y / CHUNK_SIZE;
    int chz = z / CHUNK_SIZE;

    int blx = x % CHUNK_SIZE;
    int bly = y % CHUNK_SIZE;
    int blz = z % CHUNK_SIZE;

    if(blx < 0)
    {
        blx += CHUNK_SIZE;
        chx -= 1;
    }
    if(bly < 0)
    {
        bly += CHUNK_SIZE;
        chy -= 1;
    }
    if(blz < 0)
    {
        blz += CHUNK_SIZE;
        chz -= 1;
    }

    chidx = qMakePair(qMakePair(chx, chy), chz);
    blidx = getBlockIdx(blx, bly, blz);
}

#endif // CHUNK_H
