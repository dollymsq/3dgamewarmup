#include "entity.h"

Entity::Entity(Vector3 p, Vector3 d)
{
    pos = p; dim = d;

    speed = 2;
    inair = true;
    g = 6;

    ud = 0;

    delta = 0;
    vup = 0;
    ybeforejump = pos.y;
}

void Entity::onDraw(Graphics *g)
{
    g->bindTexture(textureStr);
    g->drawEnityAsCube(pos, dim);
    g->unbindTexture();
}

void Entity::MoveInYDirection(float t)
{
    pos.y = pos.y + ud * t * speed;
    if(ud != 0 )
        ybeforejump = pos.y;
}

void Entity::Jump(float t)
{
    if(inair)
    {
        delta += vup * t - 0.5 * g * t * t; //the supposed position y right now(offset:1.5)
        vup += -g * t;

        pos.y = ybeforejump + delta;
    }
}




