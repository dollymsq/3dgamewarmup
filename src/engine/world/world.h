#ifndef WORLD_H
#define WORLD_H
#include <QKeyEvent>
#include "engine/math/camera.h"
#include "engine/world/entity.h"
#include "engine/voxel/voxelmanager.h"

class World
{
public:
    World();
    virtual ~World();

    // Concrete but overridable tick and draw
    virtual void onTick(float seconds, Camera *cam);
    virtual void onDraw(Graphics *g, GLfloat *matrix);

    // Abstract input events
    virtual void onInitialize() = 0;
    virtual int onKeyPressEvent(QKeyEvent *event) = 0;
    virtual void onKeyReleaseEvent(QKeyEvent *event) = 0;

    virtual void onMousePressEvent(QMouseEvent *event) = 0;
    virtual void onMouseReleaseEvent(QMouseEvent *event) = 0;
    virtual void restart() = 0;

    void addEntity(Entity *e);
    // Allow game code to setup the camera before drawing
    void updateCamera(Camera *cam);
    void collisionDetectBtwnEntites();

    Vector3 cylinderCollisionDetect(Cylinder c1, Cylinder c2);

protected:

    QList<Entity *> _entities; //the first entity is player
    VoxelManager *m_voxelmanager;
    int lr, fb, ud;


};

#endif // WORLD_H
