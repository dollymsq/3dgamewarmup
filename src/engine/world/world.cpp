#include "world.h"

World::World()
{
    lr = 0;
    fb = 0;
    ud = 0;
}

World::~World()
{
    delete m_voxelmanager;
    foreach(Entity *e,_entities)
        delete e;
}

void World::onTick(float seconds, Camera *cam)
{
    foreach(Entity *e,_entities)
        e->onTick(seconds);
}

void World::onDraw(Graphics *g, GLfloat *matrix)
{
    foreach(Entity *e,_entities)
        e->onDraw(g);
}

void World::updateCamera(Camera *cam)
{
    cam->update();
}

void World::collisionDetectBtwnEntites()
{
    Cylinder c1, c2;
    Vector3 mtv;
    //first pass to eliminate other entities that aren't in the display range
    for(int i = 1; i < _entities.size(); i ++)
    {
        Vector3 dis = (_entities[i]->pos - _entities[0]->pos).abs();
        if(dis.max() > 3 * CHUNK_SIZE)
        {
            delete _entities[i];
            _entities.removeAt(i);
        }
    }
    for(int i = 0; i < _entities.size(); i ++)
        for(int j = i + 1; j < _entities.size(); j ++)
        {
            c1 = _entities[i]->getCylinder();
            c2 = _entities[j]->getCylinder();

            mtv = cylinderCollisionDetect(c1, c2);

            if(mtv != Vector3(0, 0, 0))
                _entities[i]->onCollide(_entities[j], mtv);
        }

//    if(_entities.size() >= 3)
//    {
//        c1 = _entities[1]->getCylinder();
//        c2 = _entities[2]->getCylinder();

//        mtv = cylinderCollisionDetect(c1, c2);

//        if(mtv != Vector3(0, 0, 0))
//            _entities[1]->onCollide(_entities[2], mtv);
//    }

}

Vector3 World::cylinderCollisionDetect(Cylinder c1, Cylinder c2)
{
    Vector3 mtv;

    char flagx, flagy;
    //circle math
    Vector3 centraldis = (c1.pos - c2.pos);
    centraldis.y = 0;
    if(centraldis.lengthSquared() < pow((c1.radius + c2.radius), 2))
    {
        float len = (c1.pos - c2.pos).length();
        mtv = (c2.pos - c1.pos)/len * ((c1.radius + c2.radius) - len);
        mtv.y = 0;
        flagx = 'y';
    }
    else
        flagx = 'n';

    //line math
    float mtvy;
    float aRight = c2.pos.y + c2.height - c1.pos.y;
    float aLeft = c1.pos.y + c1.height - c2.pos.y;
    if(aLeft < 0 || aRight < 0)
    {
        mtvy = 0;
        flagy = 'n';
    }
    else if(aRight < aLeft)
    {
        mtvy = aRight;
        flagy = 'y';
    }
    else
    {
        mtvy = -aLeft;
        flagy = 'y';
    }

    if(flagx == 'y' && flagy == 'y')
    {
        if(mtv.lengthSquared() > mtvy * mtvy )
            return Vector3(0, mtvy, 0);
        else
            return mtv;
    }
    else
        return Vector3(0, 0, 0);
}

void World::addEntity(Entity *e)
{
    _entities.append(e);
}
