#ifndef ENTITY_H
#define ENTITY_H
#include "engine/math/vector.h"
#include "engine/math/camera.h"
#include "engine/graphics/graphics.h"

struct Cylinder
{
    Vector3 pos;
    float radius;
    float height;
    Cylinder(Vector3 p = Vector3(0,0,0), float r = 0, float h = 0) {pos = p; radius = r; height = h; }
};

enum entityType { PLAYER, ENEMY, BULLET };

class Entity
{
public:
    Entity(Vector3 p = Vector3(0,0,0), Vector3 d = Vector3(0,0,0)) ;

    virtual void onTick(float seconds) = 0;
    virtual void onDraw(Graphics *g);

    virtual void setDirection(Camera *cam, Vector3 v) = 0;
    virtual void MoveInXZPlane(float t) = 0;
    virtual void MoveInYDirection(float t);
    virtual void Jump(float t);

    Cylinder getCylinder() { return Cylinder(pos, dim.x/2*1.414, dim.y);}
    virtual void onCollide(Entity *e, Vector3 mtv) = 0;

    float vup, speed, ybeforejump, g, delta, vupMax;

    bool inair, isfrozen;
    Vector3 pos, dim, right, forward, vxz;//in XZ plane
    int ud, blood, identity, score;

protected:
    QString textureStr;
};

#endif // ENTITY_H
