#ifndef CAMERA_H
#define CAMERA_H

#include "vector.h"
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include "qgl.h"
#include "iostream"

using namespace std;

class Camera
{
public:
    Camera();
    ~Camera();

    void lookAt(float w, float h);
    void cameraOrient(int x, int y);
    void setEye(Vector3 e);
    Vector3 getEye() { return eye;}
    float vup, vs;
    bool inair;
    Vector3 right, forward;
    int mode;
    enum camera_mode { _1stPersonView, _3rdPersonView};

    void update();

    Vector3 eye, look;

private:
    Vector3 up;
    float cur_yaw, cur_pitch; // degrees
    float _w, _h;
};

#endif // CAMERA_H
