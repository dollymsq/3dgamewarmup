#include "camera.h"

Camera::Camera()
{
    eye = Vector3(0, 11.5, 0);
    up = Vector3(0, 1, 0);
    look = Vector3(0, 0, -1);
    right = Vector3(1, 0, 0);
    forward = look;

    cur_yaw = -90;
    cur_pitch = 0;

    inair = false;
    mode = _1stPersonView;
}

Camera::~Camera()
{
}

void Camera::lookAt(float w, float h)
{
    _w = w;
    _h = h;
    // set up projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(55, w / h, 0.01, 500);

    // set up modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye.x, eye.y, eye.z,
              eye.x + look.x, eye.y + look.y, eye.z + look.z,
              up.x, up.y, up.z);
//    std::cout<<eye.x<<", "<<eye.y<<", "<<eye.z<<"   eye"<<endl;
}

void Camera::update()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(55, _w / _h, 0.01, 500);

    // set up modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye.x, eye.y, eye.z,
              eye.x + look.x, eye.y + look.y, eye.z + look.z,
              up.x, up.y, up.z);
}

void Camera::cameraOrient(int x, int y)
{
    cur_pitch -= (float)y * 0.1;

    cur_pitch = min(89.99f, cur_pitch);
    cur_pitch = max(-89.99f, cur_pitch);

    cur_yaw += (float)x * 0.1;
    if(cur_yaw > 360.0f) cur_yaw -= 360.0f;
    else if(cur_yaw < -360.0f) cur_yaw += 360.0f;

    look = look.fromAngles(cur_yaw / 180.0f * M_PI, cur_pitch / 180.0f * M_PI);
    right = look.cross(up).unit();
    forward = up.cross(right);
}

void Camera::setEye(Vector3 e)
{
    switch(mode)
    {
    case _1stPersonView:
        eye = e;
        break;
    case _3rdPersonView:
        eye = e - 5 * look;
        break;
    default:
        break;
    }
    eye.y = eye.y + 1.5;
}

