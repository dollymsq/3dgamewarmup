QT += core gui opengl

TARGET = warmup
TEMPLATE = app

# If you add your own folders, add them to INCLUDEPATH and DEPENDPATH, e.g.
# INCLUDEPATH += folder1 folder2
# DEPENDPATH += folder1 folder2

INCLUDEPATH += src
DEPENDPATH += src

SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/view.cpp \
    src/engine/graphics/graphics.cpp \
    src/engine/app/screen.cpp \
    src/engine/app/application.cpp \
    src/engine/math/camera.cpp \
    src/game/app/introscreen.cpp \
    src/game/app/gamescreen.cpp \
    src/game/app/game.cpp \
    src/engine/world/world.cpp \
    src/engine/world/entity.cpp \
    src/game/world/minecraftworld.cpp \
    src/game/world/player.cpp \
    src/engine/voxel/chunk.cpp \
    src/engine/voxel/voxelmanager.cpp \
    src/game/voxel/minecraftvoxelmanager.cpp \
    src/game/world/enemy.cpp \
    src/game/world/bullet.cpp


HEADERS += src/mainwindow.h \
    src/view.h \
    src/engine/graphics/graphics.h \
    src/engine/app/screen.h \
    src/engine/app/application.h \
    src/engine/math/vector.h \
    src/engine/math/camera.h \
    src/game/app/introscreen.h \
    src/game/app/gamescreen.h \
    src/game/app/game.h \
    src/engine/world/world.h \
    src/engine/world/entity.h \
    src/game/world/minecraftworld.h \
    src/game/world/player.h \
    src/engine/voxel/chunk.h \
    src/engine/voxel/voxelmanager.h \
    src/game/voxel/minecraftvoxelmanager.h \
    src/game/world/enemy.h \
    src/game/world/bullet.h


FORMS += src/mainwindow.ui

RESOURCES += \
    src/texture.qrc

LIBS += \-lGLU \

